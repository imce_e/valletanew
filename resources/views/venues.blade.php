@extends('welcome')

@section('venuesResp')
    <div class="col-md-9 col-md-offset-1 col-sm-8 col-sm-offset-1 catResult">
        @foreach($resp as $k => $venue)
            <div class='wrapRes'><div class="col-md-10"><h4>Name: {{$venue['venue']['name']}}</h4></div>
            <div class="col-md-2"><h5><i class="fa fa-star" aria-hidden="true"></i>{{$venue['venue']["rating"]}}</h5></div>
            <div class="col-md-5 col-sm-5"><h5><i class="fa fa-globe" aria-hidden="true"></i>{{$venue['venue']['location']['country']}}</h5></div>
            <div class="col-md-5 col-sm-5"><h5 class="pointer openMap" data-lng="{{$venue['venue']['location']['lng']}}" data-lat="{{$venue['venue']['location']['lat']}}"><i class="fa fa-map" aria-hidden="true"></i>Map</h5></div></div>
            @endforeach
            <div id="map" title="Map">
                <div id="mapLocation"></div>
            </div>
    </div>
    @endsection
@section('script')
    <script>
            $( "#map" ).dialog({
                resizable: false,
                autoOpen: false,
                show: {
                    effect: "blind",
                    duration: 1000
                },
            });
           // var venue = '';
//            function api(section,venue){
//                return "https://api.foursquare.com/v2/venues/explore?v=20171017&section="+section+"&novelty=new&near="+venue+"&client_id=NSHH33T1BZ4NBTBLLNHRYQE5DX4WG1HVT3QUO0AJQ3RUJGUA&client_secret=ED02XYA5DNRSNTJKZ5PFVTIFRPS0GGZHAZ4WUE2HQQJAJ30L"
//            }

//            $('.getCat').on('click',function(e){
//                venue = this.dataset.venue;
//                $('.catResult').html('');
//                var catDig = this.dataset.category;
//                $.get(api(catDig,venue),{},function(response){
//                    $('.catResult').append("<h4>Suggestions for "+catDig+" near "+response.response.headerLocation+"</h4>");
//                    for(var j=0; j<response.response.groups[0].items.length; j++){
//                        $('.catResult').append(
//                            "<div class='wrapRes'><div class=\"col-md-10\"><h4>Name: "+response.response.groups[0].items[j].venue.name+"</h4></div>" +
//                            "<div class=\"col-md-2 \"><h5><i class=\"fa fa-star\" aria-hidden=\"true\"></i>"+response.response.groups[0].items[j].venue.rating+"</h5></div>" +
//                            "<div class=\"col-md-3 col-sm-3\"><h5><i class=\"fa fa-location-arrow\" aria-hidden=\"true\"></i>"+response.response.groups[0].items[j].venue.location.city+"</h5></div>" +
//                            "<div class=\"col-md-3 col-sm-3\"><h5><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i> "+response.response.groups[0].items[j].venue.location.address+"</h5></div>" +
//                            "<div class=\"col-md-3 col-sm-3\"><h5><i class=\"fa fa-globe\" aria-hidden=\"true\"></i> "+response.response.groups[0].items[j].venue.location.country+"</h5></div>" +
//                            "<div class=\"col-md-3 col-sm-3\"><h5 class=\"pointer openMap\" data-lng="+response.response.groups[0].items[j].venue.location.lng+" data-lat="+response.response.groups[0].items[j].venue.location.lat+"><i class=\"fa fa-map\" aria-hidden=\"true\"></i>Map</h5></div></div>");
//                    }
            $(".openMap").on('click',function(){
                var lat = this.dataset.lat,
                    lng = this.dataset.lng;
                $("#mapLocation").html('<iframe width="270" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://www.openstreetmap.org/export/embed.html?bbox='+lng+'%2C'+lat+'%2C'+lng+'%2C'+lat+'&amp;layer=mapnik&amp;marker='+lat+'%2C'+lng+'" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat='+lat+'&amp;mlon='+lng+'#map=17/'+lat+'/'+lng+'" target="_blank">View Larger Map</a></small>');
                $("#map").dialog("open");
            })
//                })
//            })
    </script>
@endsection