<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>NeimElezi</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <!-- Styles -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" rel="stylesheet" >
        <link href="/css/mystyle.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12 head">
                    <h2 class="titleTask">Neim Elezi - Task</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-3 categories">
                    <h4>Categories</h4>
                    <ul>
                        <li class="venues" data-city="valletta"><span>Valletta</span><span style="float:right;">+</span></li>
                        <li class="subCategories valletta">
                            <ul>
                                <li class="getCat" data-venue="valletta" data-category="toppicks"><i class="fa fa-trophy" aria-hidden="true"></i><span><a href="{{route ('vCategory',['vendi'=>'valletta','cat'=>'toppicks'])}}">Top Picks</a></span></li>
                                <li class="getCat" data-venue="valletta" data-category="nightlife"><i class="fa fa-glass" aria-hidden="true"></i><span><a href="{{route ('vCategory',['vendi'=>'valletta','cat'=>'food'])}}">Food</a></span></li>
                                <li class="getCat" data-venue="valletta" data-category="shopping"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span><a href="{{route ('vCategory',['vendi'=>'valletta','cat'=>'shopping'])}}">Shopping</a></span></li>
                            </ul>
                        </li>
                        <li class="venues" data-city="skopje"><span>Skopje</span><span style="float:right;">+</span></li>
                        <li class="subCategories skopje">
                            <ul>
                                <li class="getCat" data-venue="skopje" data-category="coffee"><i class="fa fa-coffee" aria-hidden="true"></i><span><a href="{{route ('vCategory',['vendi'=>'skopje','cat'=>'coffee'])}}">Coffee</a></span></li>
                                <li class="getCat" data-venue="skopje" data-category="nightlife"><i class="fa fa-glass" aria-hidden="true"></i><span><a href="{{route ('vCategory',['vendi'=>'skopje','cat'=>'nightlife'])}}">Nightlife</a></span></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                @yield('venuesResp')
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        $(function(){
            $('.venues').on('click',function(){
                var togglethis = this.dataset.city;
                $('.'+togglethis+'').toggle('fast');
            })
        })
    </script>
    @yield('script')

</html>
