<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use FoursquareApi;

class vcategory extends Controller
{
    public function categories($vendi,$cat){
        $foursquare = new FoursquareApi("NSHH33T1BZ4NBTBLLNHRYQE5DX4WG1HVT3QUO0AJQ3RUJGUA", "ED02XYA5DNRSNTJKZ5PFVTIFRPS0GGZHAZ4WUE2HQQJAJ30L");
        $endpoint = "venues/explore";
        $params = array("near"=>$vendi,"section"=>$cat);
        $response = $foursquare->GetPublic($endpoint,$params);
        $prepare = json_decode($response,true);
        //dd($prepare['response']['groups'][0]['items'][0]['venue']);
        return view('venues',['resp'=>$prepare['response']['groups'][0]['items']]);
    }
}
